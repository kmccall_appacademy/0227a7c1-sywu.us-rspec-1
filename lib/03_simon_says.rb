def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string , num=2)
  array = []
  num.times do
    array << string
  end
  
  array.join(" ")
end

def start_of_word(word , pos=0)
  word[0..pos-1]
end

def first_word(string)
  string.split[0]
end

def titleize(title)
  
  word_pool = ["an","or","and","the","over"]
  array = []
  new_string = title.split
  array << new_string[0].capitalize
  new_string[1..-1].each do |word|
    if word.length > 3 && word_pool.include?(word) == false
      array << word.capitalize
    else
      array << word
    end
  end
  
  array.join(" ")
    
end
