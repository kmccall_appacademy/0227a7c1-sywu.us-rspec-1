def add(num1 , num2)
  num1 + num2
end

def subtract(num1 , num2)
  num1 - num2
end

def sum(array)
  return 0 if array.empty?
  array.reduce(:+)
end

def multiply(arr)
  arr.reduce(:*)
end

def power(num1 , num2)
  num1 ** num2
end

def factorial(num)
  return 1 if num == 0
  (1..num).reduce(:*)
end



#
# * `multiply` which multiplies two numbers together
# * `power` which raises one number to the power of another number
# * `[factorial](http://en.wikipedia.org/wiki/Factorial)` (check
#   Wikipedia if you forgot your high school math).
