def latin_it(word)
  
  new_word = ""
  vowels = "aeiou"
  i = 0
  until vowels.include?(word[i])
    i += 1
  end
  
  if word[i] == "u" && word[i-1] == "q"
    new_word = word[i+1..-1] + word[0..i] + "ay"
  else
    new_word = word[i..-1] + word[0...i] + "ay"
  end
  
end

def translate(string)
  array = []
  string.split.each {|word| array << latin_it(word)}
  array.join(" ")
end



  # Test-driving bonus:
  # * write a test asserting that capitalized words are still capitalized (but with a different initial capital letter, of course)
  # * retain the punctuation from the original phrase
